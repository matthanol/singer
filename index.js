const Discord = require('discord.js');
const client = new Discord.Client();
const messageRouter = require('./routers/messageRouter')
const voiceRouter = require('./routers/voiceRouter')
const fs = require('fs');
const secret = getSecret();

client.on('ready', () => {
    console.log('logged in as ' + client.user.tag)
})

// handles message input
client.on('message', msg => messageRouter.handle(msg))

client.on('voiceStateUpdate', (oldMember, newMember) => {
    let newUserChannel = newMember.voiceChannel
    let oldUserChannel = oldMember.voiceChannel
  
  
    if(oldUserChannel === undefined && newUserChannel !== undefined) {
  
       voiceRouter.handle(newMember, voiceRouter.State.ENTERING)
  
    } else if(newUserChannel === undefined){
  
      // User leaves a voice channel
      voiceRouter.handle(oldMember, voiceRouter.State.LEAVING)
  
    }
  })




client.login(secret)


function getSecret() {
    try {
        return JSON.parse(fs.readFileSync('token.json')).token
    } catch (e) {
        console.log(e)
        console.log("Didn't find secrets.json")
    }

}
module.exports.getSecret = getSecret