

/**
 * Handles the first line of routing. It checks if the message was for the bot and if it was not sent by itself.
 * 
 * @param {Message} message 
 * message JSON as documented on https://discord.js.org/#/docs/main/stable/class/Message
 * 
 * @returns void
 */
const handle = function(message){
const client = message.client;
const text = message.toString();


// These functions will be called on a message where the bot is tagged 
    if(message.author.id !== client.user.id && message.isMentioned(client.user.id)){
// Further processing can be done here to branch of to your own features. make these new features in new files.



    }
}





module.exports.handle = handle;