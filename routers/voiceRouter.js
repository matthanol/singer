
/**
 * Handles the first line of routing. It checks if the message was for the bot and if it was not sent by itself.
 * 
 * @param {GuildMember} user 
 * an instance of GuildMember as defined in https://discord.js.org/#/docs/main/stable/class/GuildMember
 * this is the user doing an action in the voicechannel
 * 
 * @param {State} state
 * a state from the enum to define the action
 * 
 * 
 * @returns void
 */
const handle = function(user, state){

switch(state){
    case stateEnum.ENTERING:

        break;
    case stateEnum.LEAVING:

        break;
    case stateEnum.MUTING:

        break;
    case stateEnum.DEAFENING:

        break;
}


}





module.exports.handle = handle;


/*
* @enum {State}
*/
const stateEnum = {
ENTERING: 1,
LEAVING: 2,
MUTING: 3,
DEAFENING: 4,
}
module.exports.State = stateEnum;